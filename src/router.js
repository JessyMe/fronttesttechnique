import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from './components/User/Login';
import OrdersList from './components/Admin/OrdersList'
import Cart from './components/Cart/Cart'


Vue.use(VueRouter);

export default new VueRouter({
    mode: 'history',
    routes: [
   {
     path: '/',
     component: Login
   },
   {
     path: '/secure/admin',
     component: OrdersList,
   },
   {
     path: '/secure/admin/new_cart',
     component: Cart,
   },
   {
     path: '*',
     component: Login

   }
 ]
});